
# doxa-jhumphrey templates 

Templates written in the Liturgical Markup Language, used by Doxa when generating a liturgical website.



Use of these files is subject to the terms stated in the LICENSE file.

[Doxa](https://doxa.liml.org) is a liturgical software product from the [Orthodox Christian Mission Center](https://ocmc.org). 

[Doxa install link](https://github.com/liturgiko/doxa/releases)
